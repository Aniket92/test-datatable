import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductRoutingModule } from './product-routing.module';

import { ReactiveFormsModule } from '@angular/forms';

import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table' ;
import { MatPaginatorModule} from '@angular/material/paginator';
import { TestDataTableComponent } from './test-data-table/test-data-table.component' ;


@NgModule({
  declarations: [ProductRoutingModule.components,  TestDataTableComponent],

  imports: [
    CommonModule,
    ProductRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule
  ],
  providers: [
   
  ],
})
export class ProductModule { }
